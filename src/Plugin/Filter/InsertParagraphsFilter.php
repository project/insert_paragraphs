<?php

namespace Drupal\insert_paragraphs\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;

/**
 * Insert Paragraph Filter.
 *
 * @Filter(
 *   id = "insert_paragraphs_filter",
 *   title = @Translation("Insert Paragraphs Filter"),
 *   description = @Translation("This will replace it with paragraph"),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class InsertParagraphsFilter extends FilterBase {

  /**
   * Process function implementation.
   */
  public function process($text, $langcode) {
    $pattern = "/\[paragraph_id:(.+?)\]/i";
    preg_match_all($pattern, $text, $matches);
    foreach ($matches[1] as $k => $pid) {
      $p_html = $this->loadParagraph($pid);
      $text = str_replace($matches[0][$k], $p_html, $text);
    }
    return new FilterProcessResult($text);
  }

  /**
   * Process function loadParagraph.
   *
   * @param numeric $pid
   *  Paragraph id.
   *
   * @return The rendered element.
   */
  protected function loadParagraph($pid) {
    $entity_type = 'paragraph';
    $entity_id = $pid;
    $view_mode = 'full';

    $entity = \Drupal::entityTypeManager()->getStorage($entity_type)->load($entity_id);

    // View is the render array.
    $builder = \Drupal::entityTypeManager()->getViewBuilder($entity_type);
    $build = $builder->view($entity, $view_mode);
    // Render is the html output.
    $render = \Drupal::service('renderer')->render($build);

    return $render;
  }

}
