(function ($) {
  Drupal.behaviors.paragraphInsert = {
    attach: function (context) {
      $('.para-insert-btn').unbind().click(function(e) {
        var para_id = $(this).attr('data-para-id');
        var para_revision_id = $(this).attr('data-para-revision-id');
        var token = "[paragraph_id:" + para_id + "]"
        CKEDITOR.instances['edit-body-0-value'].insertText(token);
      });
    }
  }

})(jQuery);
